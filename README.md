# KG Inlander Product Detail V3
An initiative project and alternative host for Staging Gramedia Product Detail API V3.

Consideration:
- As an alternative host when our lovely staging is down after/outside working hour,
- As a data preview for other teams (Android, iOS, Frontend, etc.) to develop the all-new Product Detail API V3 on their project.

>"From Me To KG Inlander, with ❤️"

# Usage
- 📄 [OpenAPI Docs Page](https://kginlander.deta.dev/docs)
- 📄 [Redoc Page](https://kginlander.deta.dev/redoc)

### Post New Data
##### Meta Info
You can directly post the response that comes from GM Core Product Detail Meta Info API V3.

##### Store Info

1. Copy the response that comes from GM Core Product Detail Stores API V3,
2. Use this format to the `body`:
```
{
  "key": "<title>-<slug>",
  "stores": [{...}]  # response Stores API V3 is a dictionary list
}
```

Example:
```json
{
  "key": "ebook-careers-in-finance",
  "stores": [
    {
      "foo": "bar"
    },
    {
      "foo": "baz"
    },
  ]
}
```

> `key` is a default Deta Base `id` field

# Deployment
Thanks to [Deta Cloud](https://www.deta.sh/)!

# Contributing
To all KG Inlander who are interested in improving this project, you know what to do.
