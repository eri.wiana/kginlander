from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routes import (
    meta_info_router as MetaInfoRouter,
    store_info_router as StoreInfoRouter,
)


app = FastAPI(
    title='Product Detail V3',
    description='An initiative project and alternative host for Staging Gramedia Product Detail API V3.',
)
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=['*'],
    allow_headers=['*'],
    allow_credentials=True,
)

app.include_router(MetaInfoRouter)
app.include_router(StoreInfoRouter)
