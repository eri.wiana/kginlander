from deta import Deta
from fastapi import APIRouter, Request, status
from fastapi.responses import JSONResponse


deta = Deta()
meta_info = deta.Base('product_meta')
store_info = deta.Base('stores')


meta_info_router = APIRouter(prefix='/api/products/v3/meta_info', tags=['meta_info'])
store_info_router = APIRouter(prefix='/api/products/v3/stores', tags=['store_info'])


def __helper_serializer(data, exclude_keys=set(('key',))):
    return {k: v for k, v in data.items() if k not in exclude_keys}


# Meta Info
@meta_info_router.post('', name='New Meta Info', status_code=status.HTTP_201_CREATED)
async def insert_product_meta_info(request: Request):
    meta_info.delete('jumpsuit')
    data = await request.json()

    return __helper_serializer(meta_info.insert({'key': data.get('slug'), **data}))


@meta_info_router.get(
    '/{slug}', name='Get Meta Info Detail', status_code=status.HTTP_200_OK
)
def get_product_meta_info(slug: str):
    detail = meta_info.get(slug)
    if detail:
        return __helper_serializer(detail)
    return JSONResponse(
        content={
            'message': f'Meta Info {slug} Not Found',
            'code': status.HTTP_404_NOT_FOUND,
        },
        status_code=status.HTTP_404_NOT_FOUND,
    )


# End Meta Info


# Store Info
@store_info_router.post('', name='New Store Info', status_code=status.HTTP_201_CREATED)
async def insert_store_info(request: Request):
    data = await request.json()

    return __helper_serializer(store_info.insert(data))


@store_info_router.get(
    '/{slug}', name='Get Store Info Detail', status_code=status.HTTP_200_OK
)
def get_store_info(slug: str, title: str):
    stores = store_info.get(f'{title}-{slug}')

    if stores:
        return __helper_serializer(stores).get('stores')
    return JSONResponse(
        content={
            'message': f'Store Info {slug} - {title} Not Found',
            'code': status.HTTP_404_NOT_FOUND,
        },
        status_code=status.HTTP_404_NOT_FOUND,
    )


# End Store Info
